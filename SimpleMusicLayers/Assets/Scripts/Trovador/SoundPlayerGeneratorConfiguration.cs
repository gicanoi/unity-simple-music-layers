namespace  Trovador
{
    public static class SoundPlayerGeneratorConfiguration 
    { 
        public const string SFX__RESOURCES_PATH = "SFX";
        public const string PLAYER_PARTIAL_CLASS_PATH = "Assets/Scripts/Trovador/TrovadorSfxPlayerPartial.cs";
    }
}

