using System;
using System.Collections;
using UnityEngine;

namespace Trovador
{
    public static class InterpolationHelper 
    {
        public static IEnumerator Interpolate(float startValue, float goalValue, float duration, Action<float> writeValue)
        {
            var stepDuration = 0.016f;
            var interpolationStepsCount = CountInterpolationSteps(duration, stepDuration);
            var currentStep = 0;
            var distance = goalValue - startValue;
            var incrementPerStep = distance / interpolationStepsCount;
            var startTime = GetTime;
        
            while (currentStep < interpolationStepsCount)
            {
                if (ElapsedTime(startTime) >= stepDuration * currentStep)
                {
                    currentStep++;
                    startValue += incrementPerStep;
                    writeValue(startValue);
                    yield return null;
                }
                yield return null;
            }

            yield return null;
        }

        static double ElapsedTime(double startTime) => GetTime - startTime;

        static float CountInterpolationSteps(float duration, float stepDuration) => 
            duration / stepDuration;
    
        static double GetTime => AudioSettings.dspTime; 
        
    }
}

