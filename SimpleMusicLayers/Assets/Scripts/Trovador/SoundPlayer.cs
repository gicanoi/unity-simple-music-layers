using System;
using System.Collections.Generic;
using UnityEngine;

namespace Trovador
{

    public partial class SoundPlayer : MonoBehaviour
    {
        static AudioSourcePool audioSourcePool;
        static readonly Dictionary<string, AudioClip> loadedClips = new Dictionary<string, AudioClip>();

        void Awake()
        {
            audioSourcePool = gameObject.AddComponent<AudioSourcePool>();
            LoadAudioClips();
        }

        static AudioSource GetAudioSource() =>  audioSourcePool.GetAudioSource();

        static void LoadAudioClips()
        {
            var clips = Resources.LoadAll<AudioClip>(SoundPlayerGeneratorConfiguration.SFX__RESOURCES_PATH);

            foreach (var clip in clips)
            {
                var name = SoundPlayerGeneratorHelper.SanitizeFileName(clip);
                loadedClips.Add(name, clip);
            }
        }
    }
}
