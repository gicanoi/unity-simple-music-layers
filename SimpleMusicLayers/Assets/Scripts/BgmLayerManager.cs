using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

public class BgmLayerManager : MonoBehaviour
{
    const int MinVolume = -40;
    const int MaxVolume = 20;
    const float DefaultVolume = 0;
    const float InterpolationTime = 10;
    
    [SerializeField] AudioMixer mixer;
    
    void Start() => 
        StartCoroutine(Trovador.InterpolationHelper.Interpolate(-20, DefaultVolume, 1, value => mixer.SetFloat(GetLayerVolumeParameterName(4), value)));
   
    float GetLayerCurrentVolume(int layer)
    {
        mixer.GetFloat(GetLayerVolumeParameterName(layer) , out var volume);
        return volume;
    }
    
    static string GetLayerVolumeParameterName(int layer) => $"Layer{layer}Volume";
    
}
